Quick install Docker and DFP (on Docker) for Ubuntu

Steps:

 
1. Create your on-prem host in the CSP portal and get the API access key

2. Install git , pull the script and run it

```
sudo apt install git -y && git clone https://gitlab.com/p4r458/bloxone.git; sudo bash bloxone/setup
```

3. Input access key when prompted